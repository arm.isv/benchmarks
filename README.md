# Benchmarks

Run benchmarks on GitLab Runner infrastructure to test the following:

- CPU
- I/O
- Memory
- Network
